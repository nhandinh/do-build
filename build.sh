#!/bin/sh
echo "==============================Pulling do-ui started ====================================="
unset JAVA_HOME
cd ../do-ui
git pull
echo "===============================Pulling do-ui finished ==================================="

echo "============================Building do-common started =================================="
cd ../do-common
git pull
chmod +x gradlew
./gradlew jar
./gradlew publishToMavenLocal
echo "============================Building do-common finished ================================="

echo "============================Building do-monitor started ================================="
cd ../do-monitor
git pull
chmod +x gradlew
./gradlew bootJar
echo "============================Building do-monitor finished ================================="

echo "===============================Building do-auth started =================================="
cd ../do-auth
git pull
chmod +x gradlew
./gradlew bootJar
echo "============================Building do-auth finished ==================================="

echo "============================Building do-gateway started ================================="
cd ../do-gateway
git pull
chmod +x gradlew
./gradlew bootJar
echo "============================Building do-gateway finished ================================="

echo "=============================Building do-media started ==================================="
cd ../do-media
git pull
chmod +x gradlew
./gradlew bootJar
echo "============================Building do-media finished ==================================="

echo "============================Building do-community started ================================="
cd ../do-community
git pull
chmod +x gradlew
./gradlew bootJar
echo "===========================Building do-community finished ================================="

echo "==================Start building and publishing docker images ============================="
cd ../do-build
docker-compose build
docker-compose push
echo "===================================Build completed ========================================"