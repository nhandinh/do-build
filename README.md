#1. Login to repos

git config --global credential.helper store

docker login registry.gitlab.com/nhandinhcit

#2. Clone source code if you want to build

git clone https://gitlab.com/nhandinhcit/do-monitor.git

git clone https://gitlab.com/nhandinhcit/do-auth.git

git clone https://gitlab.com/nhandinhcit/do-gateway.git

git clone https://gitlab.com/nhandinhcit/do-ui.git

git clone https://gitlab.com/nhandinhcit/do-common.git

git clone https://gitlab.com/nhandinhcit/do-build.git



#3. To build local only, without publish to container registry. This mode assumes that you already have latest binaries in-place. It will only get existing binary and build docker images.

cd do-build

docker-compose build

#4. To build and publish images to container registry. This mode will pull latest code, build binaries, build container images and publish to container registry.

cd do-build

sh build.sh

#5. To deploy app:

cd do-build

docker-compose up
